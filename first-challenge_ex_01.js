/*  -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------


    * * * * * * * * * * * * * * * *
    *  1. C A L C U L A D O R A   *
    * * * * * * * * * * * * * * * *
    
    Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

        - El programa debe recibir dos números (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta, multiplicación 
          o división).

        - Opcional: agrega una operación que permita elevar n1 a la potencia n2.


    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------
*/

function basicCalculator(n1, n2) {
    const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
    })

    readline.question(`Selecciona el código numérico de la operación: Suma(1), Resta(2), Multiplica(3) o Divide(4): `, (input) => {
        readline.close()
        let result = 0;
        let operation = parseInt(input);
        if (operation == 1) {
            result = n1 + n2;
        } else if (operation == 2) {
            result = n1 - n2;
        } else if (operation == 3) {
            result = n1 * n2;
        } else if (operation == 4) {
            result = n1 / n2;
        }
        console.log(result);
    })
}


console.log(basicCalculator(5, 5));