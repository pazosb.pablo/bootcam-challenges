function ClosestEnemy(arr) {
  let target = [];
  let enemies = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] == 1) {
      target.push(i);
    } else if (arr[i] == 2) {
      enemies.push(i);
    }
  }
  let firstDistance = parseInt(target[0]) - parseInt(enemies[0]);
  let firstPositiveDistance = -firstDistance>0 ? -firstDistance : firstDistance;
  for (let j = 0; j < enemies.length; j++) {
    let distance = parseInt(target[0]) - parseInt(enemies[j]);
    let positiveDistance = -distance>0 ? -distance : distance;
    if (positiveDistance < firstPositiveDistance) {
      firstPositiveDistance = positiveDistance;
    }
  }
  return firstPositiveDistance
}

// keep this function call here 
//console.log(ClosestEnemy(readline()));


