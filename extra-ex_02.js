//Ordenar el array en orden creciente sin crear un nuevo array.

const numDesordenados = [4, 2, 6, 7, 1, 13, 5];

for (let i = 0; i < numDesordenados.length; i++) {
    let smaller = numDesordenados[i];
    for (let j = 0; j < numDesordenados.length; j++) {
        if (j < smaller) {
            smaller = numDesordenados[j];
        } 
    }
    console.log(smaller);
}

