const infected = [true, false, false, false, true, false, false, false, true];

function infection (infectedArray) {
    let newInfected = [];
    for (let i = 0; i < infectedArray.length; i++) {
        if (infectedArray[i+1] == true) {
            newInfected.push(true);
        } else if (infectedArray[i-1] == true) {
            newInfected.push(true);
        } else {
            newInfected.push(infectedArray[i]);
        }
    }
    return newInfected;
}

console.log(infection(infected));