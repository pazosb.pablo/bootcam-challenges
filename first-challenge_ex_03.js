/*  -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------
     
    
    * * * * * * * * * * * 
    *  3. ASINCRONÍA    *
    * * * * * * * * * * * 

    En este ejercicio se comprobará la competencia de los alumnos en el concepto de asincronía.

        - Se proporcionan 3 archivos .csv separados por comas y se deberán bajar asincronamente (promises).
    
        - A la salida se juntarán los registros de los 3 archivos en un array que será el parámetro de entrada
        de la funcion findIPbyName(array, name, surname) que buscará una entrada en el array y devolverá la IP correspondiente.

        - Una vez hallada la IP ha de mostrarse por pantalla.

        - Para llamar a la función utilizad el nombre Cari Wederell.


    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------
*/

const fs = require('fs');

const dataBase1 = fs.readFileSync('files/MOCK_DATA1.csv').toString().split('\n');
const dataBase2 = fs.readFileSync('files/MOCK_DATA2.csv').toString().split('\n');
const dataBase3 = fs.readFileSync('files/MOCK_DATA3.csv').toString().split('\n');

let arrayDataBase1 = Array.from(dataBase1);
let arrayDataBase2 = Array.from(dataBase2);
let arrayDataBase3 = Array.from(dataBase3);

let globalDataBase = arrayDataBase1.concat(arrayDataBase2, arrayDataBase3);

function findIPbyName(array, name, surname) {
    let customerIp = 0;
    for (line of array) {
        if (line.split(',')[1] == name && line.split(',')[2] == surname) {
            customerIp = line.split(',')[5];
        }
    }
    return customerIp;
}

console.log(findIPbyName(globalDataBase, 'Cari', 'Wederell'));