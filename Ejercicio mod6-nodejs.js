const axiosCacheAdapter = require('axios-cache-adapter');
const config = require('config')

const express = require('express');
const bodyParser = require('body-parser');

const moment = require('moment');

let id = 0;

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});


const logger = createLogger({
  level: 'debug',
  format: combine(
    label({ label: 'main' }), timestamp(),
    myFormat
  ),
  defaultMeta: { service: 'user-service' },
  transports: [
    new transports.File({ filename: 'error.log', level: 'error' }),
    new transports.File({ filename: 'combined.log' }),
    new transports.Console()
  ]
});


const api = axiosCacheAdapter.setup({
  cache: {
    maxAge: 0.5 * 60 * 1000
  }
})

let eventMap = {};

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));

const domainCors = config.get('domainCors');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", domainCors.join(','));
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.post('/events', (req, res) => {
  let type = req.body['type'];

  if (type === undefined) {
    res.status(400).send();
    return;
  }

  type = type.trim()

  if (type.length === 0) {
    res.status(400).send();
    return;
  }

  if (eventMap[type] !== undefined) {
    res.status(409).send();
    return;
  }

  eventMap[type] = [];

  res.send();
})

app.post('/events/:collection', (req, res) => {
  const collection = req.params['collection'];

  if (eventMap[collection] === undefined) {
    res.status(404).send();
    return
  }


  const name = req.body['name'];
  const location = req.body['location'];
  const date = req.body['date'];
  const type = req.body['type'];
  const description = req.body['description'];

  let eventData = {};

  eventData['name'] = name;
  eventData['location'] = location;
  eventData['date'] = date;
  eventData['type'] = type;
  eventData['description'] = description;

  for (let value of Object.values(eventData)) {
    if (value === undefined || value.trim().length === 0) {
      res.status(400).send();
      return
    }
  }

  eventData.data = {};

  for (let i = 0; i < eventMap[collection].length; i++) {
    if (eventMap[collection][i].name == eventData.name &&
      eventMap[collection][i].location == eventData.location &&
      eventMap[collection][i].date == eventData.date &&
      eventMap[collection][i].type == eventData.type) {
      res.status(409).send()
      return
    }
  }

  for (let key of Object.keys(req.body)) {
    if (eventData[key] === undefined) {
      const value = req.body[key].trim();

      if (value.length === 0) {
        res.status(400).send();
        return;
      }

      eventData.data[key] = value;
    }
  }


  eventMap[collection].push(eventData);

  res.send();


})

app.get('/events/:collection', (req, res) => {
  const collection = req.params['collection'];

  if (eventMap[collection] === undefined) {
    res.status(404).send();
    return;
  }

  if(req.query.location !== undefined){
    const filteredData = eventMap.filter( item => item.location === req.query.location);
    res.send(filteredData);
    }

  res.send(eventMap[req.params.collection]);
});


const port = config.get('server.port');

app.listen(port, function () {
  logger.info(`Starting eventsnts of interest application listening on port ${port}`);
});

if(req.query.location !== undefined){
    const filteredData = events.filter( item => item.location === req.query.location);
    res.send(filteredData);
    }